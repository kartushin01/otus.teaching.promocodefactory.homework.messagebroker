﻿using MassTransit;
using MassTransit.Definition;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.GivingToCustomer.Core;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Mappers;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using RabbitModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Consumers
{
    public class PromoConsumer : IConsumer<PromoCodeRabbit>
    {
        private readonly ILogger<PromoConsumer> _logger;
        
        private readonly IRarbbitService _rabbitMqService;

        public PromoConsumer(ILogger<PromoConsumer> logger, IRarbbitService rabbitMqService)
        {

            _logger = logger;
            _rabbitMqService = rabbitMqService;
        }
        public async Task Consume(ConsumeContext<PromoCodeRabbit> context)
        {

           await _rabbitMqService.GivePromoCodeToCustomer(context.Message);

           _logger.LogInformation($"Got new promocode from rabbitmq:  {context.Message.Code}, {context.Message.ServiceInfo}, {context.Message.PreferenceId}");
        }

    }

}
