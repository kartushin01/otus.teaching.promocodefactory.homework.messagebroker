﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using RabbitModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.GivingToCustomer.Core
{
    public class RabbiMqService : IRarbbitService
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;
        private readonly IRepository<Customer> _customerRepository;

        public RabbiMqService(IRepository<Preference> preferenceRepository, IRepository<PromoCode> promoCodesRepository, IRepository<Customer> customerRepository)
        {
            _preferenceRepository = preferenceRepository;
            _promoCodesRepository = promoCodesRepository;
            _customerRepository = customerRepository;
        }

        public async Task GivePromoCodeToCustomer(PromoCodeRabbit code)
        {
            //Получаем предпочтение по имени
            var preference = await _preferenceRepository.GetByIdAsync(code.PreferenceId);

            //  Получаем клиентов с этим предпочтением:
            var customers = await _customerRepository
                .GetWhere(d => d.Preferences.Any(x =>
                    x.Preference.Id == preference.Id));

            var promocode = new PromoCode();
            promocode.Id = code.Id;

            promocode.PartnerId = code.PartnerId;
            promocode.Code = code.Code;
            promocode.ServiceInfo = code.ServiceInfo;

            promocode.BeginDate = DateTime.Parse(code.BeginDate);
            promocode.EndDate = DateTime.Parse(code.EndDate);

            promocode.Preference = preference;
            promocode.PreferenceId = preference.Id;

            promocode.Customers = new List<PromoCodeCustomer>();

            foreach (var item in customers)
            {
                promocode.Customers.Add(new PromoCodeCustomer()
                {

                    CustomerId = item.Id,
                    Customer = item,
                    PromoCodeId = promocode.Id,
                    PromoCode = promocode
                });
            };

            await _promoCodesRepository.AddAsync(promocode);
        }
    }

    public interface IRarbbitService
    {
        Task GivePromoCodeToCustomer(PromoCodeRabbit code);
    }
}
