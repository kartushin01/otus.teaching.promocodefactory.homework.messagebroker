﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitModels
{
    public class PromoCodeRabbit
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string ServiceInfo { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public Guid PartnerId { get; }
        public Guid PreferenceId { get; set; }
    }
}
