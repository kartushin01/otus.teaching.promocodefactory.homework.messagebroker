﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitModels
{
    public class SubmitPartnerManagerId
    {
        public Guid PartnerManagerId { get; set; }
    }
}
