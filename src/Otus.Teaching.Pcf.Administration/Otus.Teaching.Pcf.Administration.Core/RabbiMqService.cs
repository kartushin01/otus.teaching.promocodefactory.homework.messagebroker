﻿using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Otus.Teaching.Pcf.Administration.Core
{
    public class RabbiMqService : IRarbbitService
    {
        private readonly IRepository<Employee> _repository;

        public RabbiMqService(IRepository<Employee> repository)
        {
            _repository = repository;
        }

        public async Task UpdateAppliedPromocodes(Guid patnerManagerId)
        {
            var employee = await _repository.GetByIdAsync(patnerManagerId);

            if (employee == null)
                return;

            employee.AppliedPromocodesCount++;

            await _repository.UpdateAsync(employee);
        }
    }

    public interface IRarbbitService
    {
        Task UpdateAppliedPromocodes(Guid patnerManagerId);
    }
}
