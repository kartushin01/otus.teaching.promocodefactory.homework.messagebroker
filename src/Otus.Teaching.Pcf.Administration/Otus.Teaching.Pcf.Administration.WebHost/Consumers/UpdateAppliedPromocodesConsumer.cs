﻿using MassTransit;
using Microsoft.Extensions.Logging;
using Otus.Teaching.Pcf.Administration.Core;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using RabbitModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.WebHost.Consumers
{
    public class UpdateAppliedPromocodesConsumer : IConsumer<SubmitPartnerManagerId>
    {

        private readonly IRarbbitService _mqService;

        private readonly ILogger<UpdateAppliedPromocodesConsumer> _logger;

        public UpdateAppliedPromocodesConsumer(ILogger<UpdateAppliedPromocodesConsumer> logger, IRarbbitService mqService)
        {
            _logger = logger;
            _mqService = mqService;
        }

        public async Task Consume(ConsumeContext<SubmitPartnerManagerId> context)
        {

            await _mqService.UpdateAppliedPromocodes(context.Message.PartnerManagerId);

            _logger.LogInformation($"Got new messegage from rabbitmq:  {context.Message.PartnerManagerId}");
        }
    }
}
